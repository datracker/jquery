/*
In this project, you will help feedster add dynamic behavior to their website. 
They need help adding hover functionality to their navigation menu and buttons.
If you're up for a challenge, feedster also wants help limiting their users' posts to 140 characters or less. 
You'll need to use the jQuery documentation to figure out how to implement this using the jQuery keyup method!

Source; codecademy
*/

$(document).ready(() => {
	$('.menu').on('mouseenter', () => {
    $('.nav-menu').slideDown();
  })
  
  $('.nav-menu').on('mouseleave', () => {
    $('.nav-menu').slideUp();
  })
  
  $('.btn').on('mouseenter', event => {
    $(event.currentTarget).addClass('btn-hover');
  }).on('mouseleave', event => {
    $(event.currentTarget).removeClass('btn-hover');
  })
  
  $('.postText').on('keyup', (event) => {
    $('.postText').focus();
    $post = $(event.currentTarget).val();
    $remaining = 140 - $post.length;
    if ($remaining <= 0) {
      $('.wordcount').addClass('red');
    }
    else ($remaining > 0) {
      $('.wordcount').removeClass('red');
    }
    $('.characters').html(remaining);
    
  })
}); 
