/*
In this project, you will follow step-by-step instructions to add jQuery Effects to the Trivia Card on the right. 
We want the user to be able to read the clue, view an optional hint, and select their answer.
You will add a few different effects. A hint box will slide up and down. 
When you click an incorrect answer, it will fade away and a frowny face will appear. 
When you click the correct answer, the frowny face will disappear and a smiley face will take its place.
To complete this project, you must know how to target HTML elements in jQuery, how to create event handlers, and how to use several jQuery Effects methods.
You can preview the starting state of the Trivia Card on the right.

Source: codecademy.com
*/

$(document).ready(() =>{
	$('.hint-box').on('click', () => {
    $('.hint').slideToggle(500);
  })
  $('.wrong-answer-one').on('click', () => {
    $('.wrong-answer-one').fadeOut('slow');
    $('.frown').show();
  })
  $('.wrong-answer-two').on('click', () => {
    $('.wrong-answer-two').fadeOut('fast');
    $('.frown').show();
  })
  $('.wrong-answer-three').on('click', () => {
    $('.wrong-answer-three').fadeOut();
    $('.frown').show();
  })
  $('.correct-answer').on('click', () => {
    $('.frown').hide();
    $('.smiley').show();
    $('.wrong-answer-one').fadeOut('slow');
    $('.wrong-answer-two').fadeOut('slow');
    $('.wrong-answer-three').fadeOut('slow');
  })
});